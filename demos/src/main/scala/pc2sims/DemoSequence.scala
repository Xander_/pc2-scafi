package pc2sims

import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import Builtins._


class Main1 extends BasicDemo {
  // 1 everywhere in space and time
  override def main() = 1
}
object MainApp1 extends Main1

class Main2 extends BasicDemo {
  // 5 everywhere in space and time, since operators apply locally
  override def main() = 2+3
}
object MainApp2 extends Main2

class Main3 extends BasicDemo{
  // a flat field of pairs
  def pair[X,Y](x: X,y: Y)= (x,y) // note function definition
  override def main() = pair(2,3)
}
object MainApp3 extends Main3

class Main4 extends BasicDemo{
  // a field of continuously changing random values
  // note expressions are executed continuously
  override def main() = random()
}
object MainApp4 extends Main4

class Main5 extends BasicDemo {
  // a boolean field, activated by selection and key "1"
  // note you can also drag and drop to move
  override def main() = sense1
}
object MainApp5 extends Main5

class Main6 extends BasicDemo {
  // use mux instead of "if", since we need by-value semantics here
  override def main() = mux (sense1){10}{20}
}
object MainApp6 extends Main6

class Main7 extends BasicDemo {
  // the field of devices' unique IDs
  override def main():ID = mid()
}
object MainApp7 extends Main7

class Main8 extends BasicDemo {
  // the smaller of nbrRange across neighbours
  // note "Plus" means: esclude myself
  override def main() = minHoodPlus(nbrRange)

  // note: minHoodPlus could be defined by primitive foldhood
  def myMinHood(v: => Double) =
    foldhood(Double.PositiveInfinity)(_ min _)(if (mid()==nbr(mid())) Double.PositiveInfinity else v)
}
object MainApp8 extends Main8

class Main9 extends BasicDemo {
  // the field of number of executed rounds
  override def main() = rep(0){_+1}
}
object MainApp9 extends Main9

class Main10 extends BasicDemo {
  // a time-constant random field
  // note: rep used as construct to "freeze" a field
  override def main() = rep(random()){x => x}
}
object MainApp10 extends Main10

class Main11 extends BasicDemo {
  // an increasing field with different (random) velocities
  override def main() = rep(0.0){x => x + rep(random()){y=>y} }
}
object MainApp11 extends Main11

class Main12 extends BasicDemo {
  // has a neighbour sense1 to true?
  override def main() = foldhood(false)(_ | _)(nbr(sense1))
}
object MainApp12 extends Main12

class Main13 extends BasicDemo {
  // what is this doing??
  override def main() = foldhood(0)(_ + _)(1)
}
object MainApp13 extends Main13

class Main14 extends BasicDemo {
  // a non self-stabilising gossip of "true" values
  override def main() = rep(false)(x => sense1 | foldhoodPlus(false)(_|_)(nbr(x)))
}
object MainApp14 extends Main14

class Main15 extends BasicDemo {
  // gossiping with increasing values: hop-count gradient
  override def main() = rep(Double.PositiveInfinity)(x => mux(sense1){0.0}{minHoodPlus(nbr(x)+1.0)})
}
object MainApp15 extends Main15

class Main16 extends BasicDemo {
  // gossiping with increasing values: gradient, a.k.a. distanceTo
  override def main() = rep(Double.PositiveInfinity)(x => mux(sense1){0.0}{minHoodPlus(nbr(x)+nbrRange)})
}
object MainApp16 extends Main16

class Main17 extends BasicDemo {
  // branching used to overcome an obstacle (sense2)
  def gradient(src: Boolean) = rep(Double.PositiveInfinity)(x => mux(src){0.0}{minHoodPlus(nbr(x)+nbrRange)})
  override def main() = branch(sense2){Double.PositiveInfinity}{gradient(sense1)}
}
object MainApp17 extends Main17
